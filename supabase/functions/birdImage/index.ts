import { createFlickr } from "npm:flickr-sdk"

const { flickr } = createFlickr(Deno.env.get("FLICKR_API_KEY"))

Array.prototype.random = function () {
  return this[Math.floor((Math.random()*this.length))];
}

Deno.serve(async (req) => {
  const { url } = req
  const taskPattern = new URLPattern({ pathname: '/birdImage/:species' })
  const matchingPath = taskPattern.exec(url)
  const species = matchingPath ? decodeURI(matchingPath.pathname.groups.species) : null

  const res = await flickr("flickr.photos.search", {
    text: species,
    tags: 'oiseau,oiseaux,bird,birds',
    license: "9,10",
    extras: 'license,owner_name,tags',
  })
  const img = res.photos.photo.random()

  return fetch(`https://live.staticflickr.com/${img.server}/${img.id}_${img.secret}_w.jpg`)

})
