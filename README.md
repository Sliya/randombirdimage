This is a supabase function that serves a random image of a given bird species from public domain images hosted on flickr.
# Installation

1. Run `supabase start` (see: https://supabase.com/docs/reference/cli/supabase-start)
2. Make an HTTP request:

curl -i --location --request GET 'http://127.0.0.1:54321/functions/v1/birdImage/robin' \
--header 'Authorization: Bearer <ANON_KEY>' \
--output image.jpg

# Usage

Simply call <baseURL>/:species, e.g. https://<your-supabase-url>/robin